function manageBasketDisplay() {
    //Vérifier si le panier possède au moins une caméra :
    if (localStorage.getItem("basket") === null || localStorage.getItem("basket") === "[]") {
      document.querySelector("#basketPage").parentNode.hidden = true;
    } else {
      document.querySelector("#basketPage").parentNode.hidden = false;
    }
  }
 
  function getCamerasIndex() //FindAll
  {
    fetch("http://localhost:3000/api/cameras/") //appel api 
      .then(
        response => { //fonction anonyme prend pour parametre response et return response.json
          return response.json();
        })
      .then(
        function (data) {
          for (let i = 0; i < data.length; i++) {
            getOneCamera(data[i]);
          }
  
        }
      )
  }
  
  function getOneCamera(camera) { 
    // Création des éléments
    let cameras = document.querySelector(".cameras"),

      cameraItem = document.createElement("div"),
      cameraItemBody = document.createElement("div"),
      name = document.createElement("h4"), //titre de l'article
      price = document.createElement("h5"), //sous titre article
      description = document.createElement("p"), //description
      image = document.createElement("img"), //image
      productPageLink = document.createElement("a"), //voir la page du produit
  
      urlPage = "product.html?id=" + camera._id;
  
    // Remplissage des éléments
    name.appendChild(document.createTextNode(camera.name));
    image.src = camera.imageUrl;
    price.appendChild(document.createTextNode((camera.price / 100).toLocaleString("fr") + " €"));
    description.appendChild(document.createTextNode(camera.description));
    productPageLink.appendChild(document.createTextNode("Voir la page du produit"));
    productPageLink.setAttribute('href', urlPage);
  
    //Stylisation des éléments
    productPageLink.classList.add("btn", "btn-primary");
    productPageLink.setAttribute("role", "button");
    cameraItem.classList.add("card", "border-light", "text-center", "w-25", "m-4");
    image.classList.add("card-img-top");
    cameraItemBody.classList.add("card-body","price");
    name.classList.add("card-title");
    productPageLink.classList.add("card-footer");
  
    // Placement des éléments de la camera dans son li
    cameraItemBody.appendChild(price);
    cameraItemBody.appendChild(description);
    cameraItem.appendChild(image);
    cameraItem.appendChild(name);
    cameraItem.appendChild(cameraItemBody);
    cameraItem.appendChild(productPageLink);
  
    // Placement de la camera dans le ul
    cameras.appendChild(cameraItem);
  }

  
  
  manageBasketDisplay();
  getCamerasIndex();
 