# README

![https://i.ibb.co/gFL3gjs/logo.png](https://i.ibb.co/gFL3gjs/logo.png)

# **OpenClassroom - Projet 5 - Oricono**

## **Pour commencer**

Ce projet est fait dans le cursus “Développeur web” d'OpenClassroom. (en 2021)

### **Architecture générale**

**L’application web sera composée de 4 pages :**

● une page de vue sous forme de liste, montrant tous les articles disponibles à la vente ;

● une page “produit”, qui affiche de manière dynamique l'élément sélectionné par l'utilisateur et lui permet de personnaliser le produit et de l'ajouter à son panier ;

● une page “panier” contenant un résumé des produits dans le panier, le prix total et un formulaire permettant de passer une commande. Les données du formulaire doivent être correctes et bien formatées avant d'être renvoyées au back-end. Par exemple, pas de texte dans les champs date ;

● une page de confirmation de commande, remerciant l'utilisateur pour sa commande, et indiquant le prix total et l'identifiant de commande envoyé par le serveur.


**Objectifs :**

- Valider des données issues de sources externes → ✅
- Gérer des événements JavaScript → ✅
- Créer un plan de test pour une application → ✅
- Interagir avec un web service avec JavaScript → ✅

### **Pré-requis**

- Apprenez à programmer avec JavaScript
- Écrivez du JavaScript pour le web
- Créez des sites web responsive avec Bootstrap 4

### **Installation**

- Clonez le repository, dans la source du projet, run

```
npm install
```

- Pour lancer le serveur cd /backend puis

```
cd /backend
node server
```

- Port par défaut

```
3000
```

- Si le serveur run le projet sur un autre port, pour X ou y raison, il devrais être sur ce port :

```
Listening on port 3001
```

→ ENJOY 😉

## Fait avec amour grâce à ces outils : ****

- [mon Git](https://gitlab.com/julientuyeras/) - Versioning multi-utilisateurs et cloud-stockage
- [Le site W3School](https://www.w3schools.com) - Ressourcerie fiable de code
- [L'IDE Visual Studio Code](https://code.visualstudio.com/) - IDE puissant et modulaire

- [Mon Notion](https://www.notion.so/78096dd9606b4c14b68667aa9de469b4?v=ceebddfc4fa64bd9bb2785611187fadf) - Ma gestion de projet pour Oricono

## Serveur **Back-end**

- Le back-end de Oricono est fourni. Il y a eu quelques modifications de contenus légers ( changements des lorem ipsum, ... )

### **Prérequis**

- Node / npm

### **Installation**

- Clonez sur votre ide le repository

### **URL des API**

- Caméras vintage : https://****/api/cameras